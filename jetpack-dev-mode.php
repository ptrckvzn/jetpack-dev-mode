<?php
/**
Plugin Name: Jetpack Development Mode
Description : Enable Jetpack development mode
*/

add_filter('jetpack_development_mode', '__return_true');
